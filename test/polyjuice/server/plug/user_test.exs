# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.UserTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule UserServer do
    defstruct []

    defimpl Polyjuice.Server.Protocols.BaseClientServer do
      def get_server_name(_) do
        "example.org"
      end

      def user_from_access_token(_, "an_access_token") do
        {:user, "@alice:example.org", "ADEVICE", false}
      end

      def user_from_access_token(_, _), do: nil

      def user_in_appservice_namespace?(_, _, _), do: false
    end

    defimpl Polyjuice.Server.Protocols.User do
      alias Polyjuice.Util.ClientAPIErrors

      def login_flows(_) do
        [%{"type" => "m.login.password"}, %{"type" => "m.login.token"}]
      end

      def log_in(_, {:password, "@alice:example.org", "password"}, opts) do
        device_id =
          case Keyword.get(opts, :device_id) do
            nil -> "ADEVICE"
            device_id -> device_id
          end

        {:ok,
         %{
           access_token: "an_access_token",
           device_id: device_id,
           username: "alice"
         }}
      end

      def log_in(_, {:token, "a_token"}, opts) do
        device_id =
          case Keyword.get(opts, :device_id) do
            nil -> "ADEVICE"
            device_id -> device_id
          end

        {:ok,
         %{
           access_token: "an_access_token",
           device_id: device_id,
           user_id: "@alice:example.org"
         }}
      end

      def log_in(_, _, _) do
        {:error,
         %Polyjuice.Server.Plug.MatrixError{
           message: "Forbidden",
           plug_status: 403,
           matrix_error: ClientAPIErrors.MForbidden.new("Incorrect password")
         }}
      end

      def log_out(_, "@alice:example.org", "ADEVICE"), do: nil
      def log_out(_, "@alice:example.org", "BEFWJDF"), do: nil

      def log_out(_, _, _) do
        raise "Unknown user/device"
      end

      def get_user_devices(_, "@alice:example.org"), do: ["ADEVICE", "BEFWJDF"]

      def get_user_devices(_, _) do
        raise "Unknown user"
      end

      def register(_, :user, username, _, true, opts) do
        device_id =
          case Keyword.get(opts, :device_id) do
            nil -> "ADEVICE"
            device_id -> device_id
          end

        {:ok,
         %{
           access_token: "an_access_token",
           device_id: device_id,
           username: username
         }}
      end

      def change_password(_, _, _), do: nil

      def deactivate(_, _), do: nil

      def user_interactive_auth(_, _, _, _, _, _), do: %{}
    end
  end

  test "gets login methods" do
    conn = conn(:get, "/_matrix/client/r0/login")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "flows" => [
               %{
                 "type" => "m.login.password"
               },
               %{
                 "type" => "m.login.token"
               }
             ]
           }
  end

  test "logs in a user by username" do
    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "identifier" => %{
          "type" => "m.id.user",
          "user" => "alice"
        },
        "password" => "password"
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "access_token" => "an_access_token",
             "device_id" => "ADEVICE",
             "user_id" => "@alice:example.org",
             "home_server" => "example.org"
           }
  end

  test "logs in a user by user ID" do
    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "identifier" => %{
          "type" => "m.id.user",
          "user" => "@alice:example.org"
        },
        "password" => "password"
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "access_token" => "an_access_token",
             "device_id" => "ADEVICE",
             "user_id" => "@alice:example.org",
             "home_server" => "example.org"
           }
  end

  test "logs in a user by username using deprecated properties" do
    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "user" => "alice",
        "password" => "password"
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "access_token" => "an_access_token",
             "device_id" => "ADEVICE",
             "user_id" => "@alice:example.org",
             "home_server" => "example.org"
           }
  end

  test "logs in a user by user ID using deprecated properties" do
    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "user" => "@alice:example.org",
        "password" => "password"
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "access_token" => "an_access_token",
             "device_id" => "ADEVICE",
             "user_id" => "@alice:example.org",
             "home_server" => "example.org"
           }
  end

  test "returns an error if wrong password supplied" do
    body =
      Jason.encode!(%{
        "type" => "m.login.password",
        "identifier" => %{
          "type" => "m.id.user",
          "user" => "@alice:example.org"
        },
        "password" => "not_password"
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert conn.status == 403
    assert Jason.decode!(conn.resp_body) |> Map.get("errcode") == "M_FORBIDDEN"
  end

  test "logs in a user by token" do
    body =
      Jason.encode!(%{
        "type" => "m.login.token",
        "token" => "a_token",
        "device_id" => "DEVICEID"
      })

    conn =
      conn(:post, "/_matrix/client/r0/login", body)
      |> put_req_header("content-type", "application/json")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    assert Jason.decode!(conn.resp_body) == %{
             "access_token" => "an_access_token",
             "device_id" => "DEVICEID",
             "user_id" => "@alice:example.org",
             "home_server" => "example.org"
           }
  end

  test "logs out a user" do
    conn =
      conn(:post, "/_matrix/client/r0/logout", "{}")
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    {status, _, _} = sent_resp(conn)

    assert status == 200
  end

  test "logs out all user devices" do
    conn =
      conn(:post, "/_matrix/client/r0/logout/all", "{}")
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.UserTest.UserServer{}
      )

    {status, _, _} = sent_resp(conn)

    assert status == 200
  end
end
