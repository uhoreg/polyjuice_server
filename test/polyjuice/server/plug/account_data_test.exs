# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.AccountDataTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule Server do
    defstruct []

    defimpl Polyjuice.Server.Protocols.BaseClientServer do
      def get_server_name(_) do
        "example.org"
      end

      def user_from_access_token(_, "an_access_token") do
        {:user, "@alice:example.org", "ADEVICE", false}
      end

      def user_from_access_token(_, _), do: nil

      def user_in_appservice_namespace?(_, _, _), do: false
    end

    defimpl Polyjuice.Server.Protocols.AccountData do
      def set(_, "@alice:example.org", nil, "ca.uhoreg.custom", %{"foo" => "bar"}), do: :ok
      def set(_, "@alice:example.org", "!room_id", "ca.uhoreg.custom", %{"foo" => "bar"}), do: :ok

      def get(_, "@alice:example.org", nil, "ca.uhoreg.custom"), do: {:ok, %{"foo" => "bar"}}

      def get(_, "@alice:example.org", "!room_id", "ca.uhoreg.custom"),
        do: {:ok, %{"foo" => "bar"}}

      def get(_, "@alice:example.org", _, _),
        do: {
          :error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Not Found",
            plug_status: 404,
            matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
          }
        }
    end
  end

  test "get user account data" do
    conn =
      conn(:get, "/_matrix/client/r0/user/%40alice:example.org/account_data/ca.uhoreg.custom")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.AccountDataTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == ~S({"foo":"bar"})
  end

  test "get non-existent user account data" do
    conn =
      conn(
        :get,
        "/_matrix/client/r0/user/%40alice:example.org/account_data/ca.uhoreg.does_not_exist"
      )
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.AccountDataTest.Server{}
      )

    assert conn.status == 404
  end

  test "get user room account data" do
    conn =
      conn(
        :get,
        "/_matrix/client/r0/user/%40alice:example.org/rooms/%21room_id/account_data/ca.uhoreg.custom"
      )
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.AccountDataTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == ~S({"foo":"bar"})
  end

  test "getting data for another user fails" do
    conn =
      conn(:get, "/_matrix/client/r0/user/%40bob:example.org/account_data/ca.uhoreg.custom")
      |> put_req_header("authorization", "Bearer an_access_token")

    assert_raise Plug.Conn.WrapperError, fn ->
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.AccountDataTest.Server{}
      )
    end
  end

  test "set user account data" do
    conn =
      conn(
        :put,
        "/_matrix/client/r0/user/%40alice:example.org/account_data/ca.uhoreg.custom",
        ~S({"foo":"bar"})
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.AccountDataTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == "{}"
  end

  test "set user room account data" do
    conn =
      conn(
        :put,
        "/_matrix/client/r0/user/%40alice:example.org/rooms/%21room_id/account_data/ca.uhoreg.custom",
        ~S({"foo":"bar"})
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.AccountDataTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == "{}"
  end
end
