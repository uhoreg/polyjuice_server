# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.RoomTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule Server do
    defstruct []

    defimpl Polyjuice.Server.Protocols.BaseClientServer do
      def get_server_name(_) do
        "example.org"
      end

      def user_from_access_token(_, "an_access_token") do
        {:user, "@alice:example.org", "ADEVICE", false}
      end

      def user_from_access_token(_, _), do: nil

      def user_in_appservice_namespace?(_, _, _), do: false
    end

    defimpl Polyjuice.Server.Protocols.Room do
      def create(_, _), do: {:ok, "!room_id:example.org"}

      def join(_, "@alice:example.org", "!room_id:example.org", []), do: :ok

      def send_event(
            _,
            "@alice:example.org",
            "ADEVICE",
            "!room_id:example.org",
            "m.room.message",
            nil,
            "txnid",
            %{"body" => "Hello world!"}
          ),
          do: {:ok, "$event_id"}
    end
  end

  test "join room" do
    conn =
      conn(
        :post,
        "/_matrix/client/r0/join/!room_id:example.org",
        "{}"
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.RoomTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == ~S({"room_id":"!room_id:example.org"})
  end

  test "rooms join" do
    conn =
      conn(
        :post,
        "/_matrix/client/r0/rooms/!room_id:example.org/join",
        "{}"
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.RoomTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == ~S({"room_id":"!room_id:example.org"})
  end

  test "rooms send event" do
    conn =
      conn(
        :put,
        "/_matrix/client/r0/rooms/!room_id:example.org/send/m.room.message/txnid",
        ~S({"body":"Hello world!"})
      )
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer an_access_token")

    conn =
      Polyjuice.Server.Plug.Matrix.call(
        conn,
        server: %Polyjuice.Server.Plug.RoomTest.Server{}
      )

    assert conn.status == 200
    assert conn.resp_body == ~S({"event_id":"$event_id"})
  end
end
