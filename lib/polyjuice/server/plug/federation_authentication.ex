# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.FederationAuthentication do
  @moduledoc """
  Verifies that a federation request is authenticated.

  The origin server will be stored in `conn.assigns[:origin]`.

  FIXME: this currently only works properly with GET requests
  """

  defmodule NotAuthenticatedError do
    defexception message: "Federation request was not properly signed",
                 plug_status: 403,
                 matrix_error: Polyjuice.Util.ClientAPIErrors.MUnauthorized.new()
  end

  alias Plug.Conn
  @behaviour Plug

  require Logger

  @impl Plug
  def init(opts) do
    opts
  end

  @impl Plug
  def call(conn, opts) do
    server = opts[:server]

    method = conn.method

    uri =
      case conn.query_string do
        "" -> conn.request_path
        _ -> "#{conn.request_path}?#{conn.query_string}"
      end

    # FIXME:
    body = nil

    with [authorization] <- Conn.get_req_header(conn, "authorization"),
         {:ok, origin} <-
           Polyjuice.Server.Federation.Authentication.verify(
             server,
             method,
             uri,
             body,
             authorization
           ) do
      Logger.debug("received request from #{origin}")
      put_in(conn.assigns[:origin], origin)
    else
      _ -> raise NotAuthenticatedError
    end
  end

  def federation_authentication(conn, opts), do: call(conn, opts)
end
