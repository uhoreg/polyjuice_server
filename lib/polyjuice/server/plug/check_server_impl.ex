# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.CheckServerImpl do
  defmodule NotImplementedError do
    defexception message: "Endpoint not implemented",
                 plug_status: 400,
                 matrix_error: Polyjuice.Util.ClientAPIErrors.MUnknown.new()
  end

  @behaviour Plug

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, opts) do
    server = opts[:server]
    protocol = opts[:check_impl_protocol]

    if apply(protocol, :impl_for, [server]) == nil do
      case opts[:check_impl_default_response] do
        {status, err} -> raise NotImplementedError, plug_status: status, matrix_error: err
        _ -> raise NotImplementedError
      end
    else
      conn
    end
  end
end
