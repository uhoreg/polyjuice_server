# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.Client do
  use Plug.Router
  use Plug.ErrorHandler

  plug(:match)
  plug(:cors)
  plug(:dispatch, builder_opts())

  def cors(conn, _opts) do
    conn
    |> put_resp_header("access-control-allow-origin", "*")
    |> put_resp_header("access-control-allow-methods", "GET, POST, PUT, DELETE, OPTIONS")
    |> put_resp_header(
      "access-control-allow-headers",
      "X-Requested-With, Content-Type, Authorization"
    )
  end

  options(_, do: send_resp(conn, 204, ""))

  get "/versions" do
    versions = %{
      # we can't really claim to support anything yet...
      "versions" => []
    }

    conn
    |> put_resp_content_type("application/json")
    |> put_resp_header("cache-control", "max-age=3600, public")
    |> send_resp(200, Jason.encode!(versions))
  end

  Enum.each(["/r0", "/v3"], fn pfx ->
    # account data

    get "#{pfx}/user/:user_id/account_data/:type" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.AccountData.GetUserAccountData, opts)
    end

    put "#{pfx}/user/:user_id/account_data/:type" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.AccountData.PutUserAccountData, opts)
    end

    get "#{pfx}/user/:user_id/rooms/:room_id/account_data/:type" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.AccountData.GetUserAccountData, opts)
    end

    put "#{pfx}/user/:user_id/rooms/:room_id/account_data/:type" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.AccountData.PutUserAccountData, opts)
    end

    # base

    get "#{pfx}/account/whoami" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.Client.GetAccountWhoami, opts)
    end

    # device keys

    get "#{pfx}/keys/query" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.DeviceKey.Client.GetKeysChanges, opts)
    end

    post "#{pfx}/keys/claim" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.DeviceKey.Client.PostKeysClaim, opts)
    end

    post "#{pfx}/keys/device_signing/upload" do
      Plug.forward(
        conn,
        [],
        Polyjuice.Server.Plug.DeviceKey.Client.PostKeysDeviceSigningUpload,
        opts
      )
    end

    post "#{pfx}/keys/query" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.DeviceKey.Client.PostKeysQuery, opts)
    end

    post "#{pfx}/keys/upload" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.DeviceKey.Client.PostKeysUpload, opts)
    end

    post "#{pfx}/keys/signatures/upload" do
      Plug.forward(
        conn,
        [],
        Polyjuice.Server.Plug.DeviceKey.Client.PostKeysSignaturesUpload,
        opts
      )
    end

    # key backup

    get "#{pfx}/room_keys/keys" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysKeys, opts)
    end

    put "#{pfx}/room_keys/keys" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysKeys, opts)
    end

    delete "#{pfx}/room_keys/keys" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysKeys, opts)
    end

    get "#{pfx}/room_keys/keys/:room_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysKeys, opts)
    end

    put "#{pfx}/room_keys/keys/:room_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysKeys, opts)
    end

    delete "#{pfx}/room_keys/keys/:room_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysKeys, opts)
    end

    get "#{pfx}/room_keys/keys/:room_id/:session_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysKeys, opts)
    end

    put "#{pfx}/room_keys/keys/:room_id/:session_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysKeys, opts)
    end

    delete "#{pfx}/room_keys/keys/:room_id/:session_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysKeys, opts)
    end

    get "#{pfx}/room_keys/version" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysVersion, opts)
    end

    post "#{pfx}/room_keys/version" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PostRoomKeysVersion, opts)
    end

    get "#{pfx}/room_keys/version/:version" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysVersion, opts)
    end

    put "#{pfx}/room_keys/version/:version" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysVersion, opts)
    end

    delete "#{pfx}/room_keys/version/:version" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysVersion, opts)
    end

    # pub sub

    get "#{pfx}/sync" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.PubSub.Sync, opts)
    end

    post "#{pfx}/user/:user_id/filter" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.PubSub.PostFilter, opts)
    end

    get "#{pfx}/user/:user_id/filter/filter_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.PubSub.GetFilter, opts)
    end

    # push rules

    get "#{pfx}/pushrules/" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.PushRule.GetPushrules, opts)
    end

    get "#{pfx}/pushrules/:scope/" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.PushRule.GetPushrules, opts)
    end

    get "#{pfx}/pushrules/:scope/:kind/:rule_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.PushRule.GetPushrules, opts)
    end

    # rooms

    post "#{pfx}/join/:room_id_or_alias" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.Room.PostJoin, opts)
    end

    post "#{pfx}/rooms/:room_id/invite" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.Room.PostRoomsInvite, opts)
    end

    post "#{pfx}/rooms/:room_id/join" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.Room.PostRoomsJoin, opts)
    end

    get "#{pfx}/rooms/:room_id/members" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.Room.GetRoomsMembers, opts)
    end

    put "#{pfx}/rooms/:room_id/send/:event_type/:txn_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.Room.PutRoomsSend, opts)
    end

    # room directory

    get "#{pfx}/publicRooms" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.RoomDirectory.Client, opts)
    end

    post "#{pfx}/publicRooms" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.RoomDirectory.Client, opts)
    end

    # send to device

    put "#{pfx}/sendToDevice/:event_type/:txn_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.SendToDevice.PutSendToDevice, opts)
    end

    # user

    get "#{pfx}/devices" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.GetDevices, opts)
    end

    get "#{pfx}/devices/:device_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.GetDevice, opts)
    end

    get "#{pfx}/login" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.GetLogin, opts)
    end

    post "#{pfx}/login" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.PostLogin, opts)
    end

    post "#{pfx}/logout" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.PostLogout, opts)
    end

    post "#{pfx}/logout/all" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.PostLogoutAll, opts)
    end

    get "#{pfx}/profile/:user_id" do
      Plug.forward(conn, [], Polyjuice.Server.Plug.User.GetProfile, opts)
    end
  end)

  # unstable endpoints

  # device keys - cross-signing
  # @unstable-prefix MSC1756 with-stable

  post "unstable/keys/device_signing/upload" do
    Plug.forward(
      conn,
      [],
      Polyjuice.Server.Plug.DeviceKey.Client.PostKeysDeviceSigningUpload,
      opts
    )
  end

  post "unstable/keys/signatures/upload" do
    Plug.forward(
      conn,
      [],
      Polyjuice.Server.Plug.DeviceKey.Client.PostKeysSignaturesUpload,
      opts
    )
  end

  # key backup
  # @unstable-prefix MSC1219 with-stable

  get "unstable/room_keys/keys" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysKeys, opts)
  end

  put "unstable/room_keys/keys" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysKeys, opts)
  end

  delete "unstable/room_keys/keys" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysKeys, opts)
  end

  get "unstable/room_keys/keys/:room_id" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysKeys, opts)
  end

  put "unstable/room_keys/keys/:room_id" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysKeys, opts)
  end

  delete "unstable/room_keys/keys/:room_id" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysKeys, opts)
  end

  get "unstable/room_keys/keys/:room_id/:session_id" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysKeys, opts)
  end

  put "unstable/room_keys/keys/:room_id/:session_id" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysKeys, opts)
  end

  delete "unstable/room_keys/keys/:room_id/:session_id" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysKeys, opts)
  end

  get "unstable/room_keys/version" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysVersion, opts)
  end

  post "unstable/room_keys/version" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PostRoomKeysVersion, opts)
  end

  get "unstable/room_keys/version/:version" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.GetRoomKeysVersion, opts)
  end

  put "unstable/room_keys/version/:version" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.PutRoomKeysVersion, opts)
  end

  delete "unstable/room_keys/version/:version" do
    Plug.forward(conn, [], Polyjuice.Server.Plug.KeyBackup.DeleteRoomKeysVersion, opts)
  end

  match _ do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(400, ~S({"errcode":"M_UNRECOGNIZED","error":"Unrecognized request"}))
  end

  def handle_errors(conn, err) do
    Polyjuice.Server.Plug.Matrix.handle_errors(conn, err)
  end

  defmodule GetAccountWhoami do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:whoami, builder_opts())

    def whoami(conn, _opts) do
      res =
        case conn.assigns[:user] do
          %{application_service: false, user_id: user_id, device_id: device_id, guest: guest} ->
            if guest do
              %{
                "user_id" => user_id,
                "device_id" => device_id,
                "org.matrix.msc3069.is_guest" => true
              }
            else
              %{
                "user_id" => user_id,
                "device_id" => device_id
              }
            end

          %{application_service: true, user_id: user_id} ->
            %{"user_id" => user_id}
        end

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(res))
    end
  end
end
