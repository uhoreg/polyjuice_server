# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.RoomDirectory do
  defmodule Federation do
    use Plug.Builder

    import Polyjuice.Server.Plug.FederationAuthentication, only: [federation_authentication: 2]
    plug(:federation_authentication, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:check_impl, builder_opts())
    plug(:public_rooms, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.RoomDirectory},
          {:check_impl_default_response,
           {200, %{"chunk" => [], "total_room_count_estimate" => 0}}}
          | opts
        ]
      )
    end

    def public_rooms(conn, opts) do
      server = opts[:server]

      {conn, params} =
        case conn.method do
          "GET" ->
            conn = fetch_query_params(conn)
            include_all_networks = Map.get(conn.query_params, "include_all_networks", false)

            {conn,
             %{
               limit: Map.get(conn.query_params, "limit", "0") |> String.to_integer(),
               since: Map.get(conn.query_params, "since"),
               third_party_instance_id:
                 if include_all_networks do
                   :all
                 else
                   Map.get(conn.query_params, "third_party_instance_id")
                 end,
               filter: nil
             }}

          "POST" ->
            include_all_networks = Map.get(conn.body_params, "include_all_networks", false)

            {conn,
             %{
               limit: Map.get(conn.body_params, "limit", "0") |> String.to_integer(),
               since: Map.get(conn.body_params, "since"),
               third_party_instance_id:
                 if include_all_networks do
                   :all
                 else
                   Map.get(conn.body_params, "third_party_instance_id")
                 end,
               filter: Map.get(conn.body_params, "filter")
             }}

          _ ->
            # FIXME:
            raise Polyjuice.Server.Plug.CheckServerImpl.NotImplementedError
        end

      resp =
        Polyjuice.Server.Protocols.RoomDirectory.public_rooms(
          server,
          {:server, conn.assigns[:origin]},
          params.third_party_instance_id,
          params.filter,
          params.since,
          params.limit
        )

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(resp))
    end
  end

  defmodule Client do
    use Plug.Builder

    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:check_impl, builder_opts())
    plug(:public_rooms, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.RoomDirectory},
          {:check_impl_default_response,
           {200, %{"chunk" => [], "total_room_count_estimate" => 0}}}
          | opts
        ]
      )
    end

    def public_rooms(conn, opts) do
      server = opts[:server]
      conn = fetch_query_params(conn)

      {conn, params} =
        case conn.method do
          "GET" ->
            {conn,
             %{
               limit: Map.get(conn.query_params, "limit", "0") |> String.to_integer(),
               server: Map.get(conn.query_params, "server"),
               since: nil,
               third_party_instance_id: nil,
               filter: nil
             }}

          "POST" ->
            conn = Polyjuice.Server.Plug.ClientAuthentication.call(conn, opts)
            include_all_networks = Map.get(conn.body_params, "include_all_networks", false)

            {conn,
             %{
               limit: Map.get(conn.body_params, "limit", "0") |> String.to_integer(),
               server: Map.get(conn.query_params, "server"),
               since: Map.get(conn.body_params, "since"),
               third_party_instance_id:
                 if include_all_networks do
                   :all
                 else
                   Map.get(conn.body_params, "third_party_instance_id")
                 end,
               filter: Map.get(conn.body_params, "filter")
             }}

          _ ->
            # FIXME:
            raise Polyjuice.Server.Plug.CheckServerImpl.NotImplementedError
        end

      resp =
        if params.server == nil or
             params.server == Polyjuice.Server.Protocols.BaseFederation.get_server_name(server) do
          source =
            case conn.assigns[:user] do
              %{user_id: user_id} -> {:user, user_id}
              _ -> :anonymous
            end

          Polyjuice.Server.Protocols.RoomDirectory.public_rooms(
            server,
            source,
            params.third_party_instance_id,
            params.filter,
            params.since,
            params.limit
          )
        else
          if Polyjuice.Server.Federation.Client.API.impl_for(server) != nil do
            Polyjuice.Server.Federation.Client.RoomDirectory.public_rooms(
              server,
              params.server,
              limit: params.limit,
              third_party_instance_id: params.third_party_instance_id,
              since: params.since,
              filter: params.filter
            )
          else
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Not found",
              plug_status: 404,
              matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
          end
        end

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(resp))
    end
  end
end
