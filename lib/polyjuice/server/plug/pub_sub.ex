# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.PubSub do
  defmodule Sync do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication_guests_allowed: 2]

    plug(:client_authentication_guests_allowed, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:sync, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.PubSub}
          | opts
        ]
      )
    end

    def sync(conn, opts) do
      conn = fetch_query_params(conn)

      server = opts[:server]
      user = conn.assigns[:user]

      query_params = conn.query_params

      filter =
        case Map.get(query_params, "filter") do
          nil ->
            %{}

          <<0x7B, _rest::binary>> = filter ->
            case Jason.decode(filter) do
              {:ok, filter} ->
                filter

              {:error, _} ->
                raise Polyjuice.Server.Plug.MatrixError,
                  message: "Invalid parameter: filter",
                  plug_status: 400,
                  matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new()
            end

          filter_id ->
            case Polyjuice.Server.Protocols.PubSub.get_filter(server, user.user_id, filter_id) do
              {:ok, filter} ->
                filter

              {:error, _} ->
                # FIXME: what's the right error code for this?
                raise Polyjuice.Server.Plug.MatrixError,
                  message: "No such filter",
                  plug_status: 400,
                  matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new()
            end
        end

      full_state =
        case Map.get(query_params, "full_state", "false") do
          "true" ->
            true

          "false" ->
            false

          _ ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Invalid parameter: full_state",
              plug_status: 400,
              matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new()
        end

      set_presence =
        case Map.get(query_params, "set_presence", "online") do
          "offline" ->
            :offline

          "online" ->
            :online

          "unavailable" ->
            :unavailable

          _ ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Invalid parameter: set_presence",
              plug_status: 400,
              matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new()
        end

      since =
        case Map.fetch(query_params, "since") do
          {:ok, since} ->
            case Polyjuice.Server.SyncToken.parse(since) do
              {:ok, t} ->
                t

              _ ->
                raise Polyjuice.Server.Plug.MatrixError,
                  message: "Invalid parameter: set_since",
                  plug_status: 400,
                  matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new()
            end

          _ ->
            nil
        end

      timeout =
        with param <- Map.get(query_params, "timeout", "0"),
             {t, ""} <- Integer.parse(param) do
          min(t, 120_000)
        else
          _ ->
            raise Polyjuice.Server.Plug.MatrixError,
              message: "Invalid parameter: timeout",
              plug_status: 400,
              matrix_error: Polyjuice.Util.ClientAPIErrors.MInvalidParam.new()
        end

      # FIXME: do something with full_state, set_presence

      case Polyjuice.Server.Protocols.PubSub.await_sync(
             server,
             user.user_id,
             user.device_id,
             since,
             timeout,
             filter
           ) do
        {nil, resp} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(Map.put(resp, "next_batch", "")))

        {token, resp} ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            200,
            Jason.encode!(
              Map.put(resp, "next_batch", Polyjuice.Server.SyncToken.to_string(token))
            )
          )

        :logout ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(401, Jason.encode!(Polyjuice.Util.ClientAPIErrors.MUnknownToken.new()))

        :soft_logout ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            401,
            Jason.encode!(
              Polyjuice.Util.ClientAPIErrors.MUnknownToken.new("Soft logout", soft_logout: true)
            )
          )
      end
    end
  end

  defmodule PostFilter do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:add_filter, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.PubSub}
          | opts
        ]
      )
    end

    def add_filter(conn, opts) do
      server = opts[:server]
      user_id = conn.assigns[:user].user_id
      # FIXME: we probably want a size limit on the filter
      if conn.path_params["user_id"] != user_id do
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(
          403,
          Jason.encode!(
            Polyjuice.Util.ClientAPIErrors.MInvalidParam.new("Cannot set another user's filter")
          )
        )
      else
        filter_id =
          Polyjuice.Server.Protocols.PubSub.add_filter(server, user_id, conn.body_params)

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(
          200,
          Jason.encode!(%{"filter_id" => filter_id})
        )
      end
    end
  end

  defmodule GetFilter do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(:check_impl, builder_opts())
    plug(:get_filter, builder_opts())

    def check_impl(conn, opts) do
      Polyjuice.Server.Plug.CheckServerImpl.call(
        conn,
        [
          {:check_impl_protocol, Polyjuice.Server.Protocols.PubSub}
          | opts
        ]
      )
    end

    def get_filter(conn, opts) do
      server = opts[:server]
      user_id = conn.assigns[:user].user_id

      if conn.path_params["user_id"] != user_id do
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(
          403,
          Jason.encode!(
            Polyjuice.Util.ClientAPIErrors.MInvalidParam.new("Cannot get another user's filter")
          )
        )
      else
        case Polyjuice.Server.Protocols.PubSub.get_filter(
               server,
               user_id,
               conn.path_params["filter_id"]
             ) do
          {:ok, filter} ->
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(200, Jason.encode!(filter))

          {:error, %Polyjuice.Server.Plug.MatrixError{} = err} ->
            conn
            |> put_resp_content_type("application/json")
            |> send_resp(err.plug_status, Jason.encode!(err.matrix_error))
        end
      end
    end
  end
end
