# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Plug.ClientAuthentication do
  @moduledoc """
  Verifies that a client request is authenticated.

  The user info will be stored in `conn.assigns[:user]`.  The user info is a
  map that contains the following properties:

  - `user_id`: (string)
  - `device_id`: (string)
  - `guest`: (boolean)
  - `application_service`: (boolean)

  If the endpoint should allow guests, use the
  `Polyjuice.Server.Plug.ClientAuthentication.GuestsAllowed` plug.  If
  authentication is optional, use the
  `Polyjuice.Server.Plug.ClientAuthentication.Optional` plug.

  Since `Plug.Builder` can only use `builder_opts` with function plugs, the
  `client_authentication`, `client_authentication_guests_allowed` and
  `client_authentication_optional` helper functions are provided.

  """

  defmodule NotAuthenticatedError do
    defexception [:message, :matrix_error, plug_status: 401]
  end

  defmodule Optional do
    @moduledoc """
    Authentication is optional.

    If the client provides authentication, then the user information is retrieved.
    """

    alias Plug.Conn
    alias Polyjuice.Util.ClientAPIErrors
    @behaviour Plug

    @impl Plug
    def init(opts) do
      opts
    end

    @impl Plug
    def call(conn, opts) do
      conn = Conn.fetch_query_params(conn)
      server = opts[:server]

      access_token =
        case Conn.get_req_header(conn, "authorization") do
          [<<"Bearer ", token::binary>>] ->
            String.trim(token)

          _ ->
            Map.get(conn.query_params, "access_token")
        end

      conn = put_in(conn.assigns[:access_token], access_token)

      if access_token == nil do
        conn
      else
        user_info =
          Polyjuice.Server.Protocols.BaseClientServer.user_from_access_token(
            server,
            access_token
          )

        case user_info do
          {:user, user_id, device_id, guest} ->
            put_in(
              conn.assigns[:user],
              %{
                user_id: user_id,
                device_id: device_id,
                guest: guest,
                application_service: false
              }
            )

          {:application_service, user_id} ->
            case Map.get(conn.query_params, "user_id") do
              nil ->
                put_in(
                  conn.assigns[:user],
                  %{
                    user_id: user_id,
                    device_id: nil,
                    guest: false,
                    application_service: true
                  }
                )

              asserted_user_id ->
                if Polyjuice.Server.Protocols.BaseClientServer.user_in_appservice_namespace?(
                     server,
                     user_id,
                     asserted_user_id
                   ) do
                  put_in(
                    conn.assigns[:user],
                    %{
                      user_id: asserted_user_id,
                      device_id: nil,
                      guest: false,
                      application_service: true
                    }
                  )
                else
                  raise NotAuthenticatedError,
                    message: "Asserted user ID does not belong to AS.",
                    matrix_error:
                      ClientAPIErrors.MExclusive.new("Asserted user ID does not belong to you")
                end
            end

          {:soft_logout, _user_id, _device_id, _guest} ->
            raise NotAuthenticatedError,
              message: "User soft-logged out.",
              matrix_error: ClientAPIErrors.MUnknownToken.new("Soft logout", soft_logout: true)

          nil ->
            raise NotAuthenticatedError,
              message: "Unknown access token",
              matrix_error: ClientAPIErrors.MUnknownToken.new()
        end
      end
    end
  end

  defmodule GuestsAllowed do
    alias Plug.Conn
    alias Polyjuice.Util.ClientAPIErrors
    @behaviour Plug

    @impl Plug
    def init(opts) do
      opts
    end

    @impl Plug
    def call(conn, opts) do
      conn = Polyjuice.Server.Plug.ClientAuthentication.Optional.call(conn, opts)

      if conn.assigns[:access_token] == nil do
        raise NotAuthenticatedError,
          message: "Missing access token",
          matrix_error: ClientAPIErrors.MMissingToken.new()
      else
        conn
      end
    end

    def client_authentication_guests_allowed(conn, opts), do: call(conn, opts)
  end

  alias Plug.Conn
  @behaviour Plug

  require Logger

  @impl Plug
  def init(opts) do
    opts
  end

  @impl Plug
  def call(conn, opts) do
    conn = Polyjuice.Server.Plug.ClientAuthentication.GuestsAllowed.call(conn, opts)

    case conn.assigns[:user].guest do
      true ->
        raise NotAuthenticatedError,
          message: "Guest access is not allowed",
          matrix_error: Polyjuice.Util.ClientAPIErrors.MGuestAccessForbidden.new()

      false ->
        conn
    end
  end

  def client_authentication(conn, opts), do: call(conn, opts)

  def client_authentication_guests_allowed(conn, opts),
    do: Polyjuice.Server.Plug.ClientAuthentication.GuestsAllowed.call(conn, opts)

  def client_authentication_optional(conn, opts),
    do: Polyjuice.Server.Plug.ClientAuthentication.Optional.call(conn, opts)

  defmodule UserInteractiveAuth do
    # NOTE: body parser must be called before this.  If the endpoint requires
    # users to be logged in, then
    # `Polyjuice.Server.Plug.ClientAuthentication(.GuestsAllowed)` should also
    # be called before this.
    alias Plug.Conn
    @behaviour Plug

    @impl Plug
    def init(opts) do
      opts
    end

    @impl Plug
    def call(conn, opts) do
      conn = Conn.fetch_query_params(conn)
      server = opts[:server]

      if Polyjuice.Server.Protocols.User.impl_for(server) == nil do
        conn
        |> Conn.put_resp_content_type("application/json")
        |> Conn.send_resp(401, ~S({"errcode":"M_FORBIDDEN,"error":"Not implemented"}))
        |> Conn.halt()
      else
        case Polyjuice.Server.Protocols.User.user_interactive_auth(
               server,
               Map.get(conn.assigns, :access_token),
               opts[:ui_auth_type],
               opts[:ui_auth_endpoint],
               Map.get(conn.body_params, "auth"),
               Map.delete(conn.body_params, "auth")
             ) do
          :ok ->
            conn

          {:cont, resp} ->
            conn
            |> Conn.put_resp_content_type("application/json")
            |> Conn.send_resp(401, Jason.encode!(resp))
            |> Conn.halt()

          {:error, %{plug_status: status, matrix_error: err}} ->
            conn
            |> Conn.put_resp_content_type("application/json")
            |> Conn.send_resp(status, Jason.encode!(err))
            |> Conn.halt()
        end
      end
    end
  end
end
