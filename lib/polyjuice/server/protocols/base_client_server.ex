# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.BaseClientServer do
  @moduledoc """
  Base client-server functionality.

  Any server that handles client requests must implement this protocol.

  ## Access Tokens

  Clients requests are authenticated by used of an access token that is
  provided to the client when they log in.  Each access token is associated
  with a user ID and device ID.  Alternatively, an access token may belong to
  an application service that is set in some other way.

  For authenticating requests and ensuring that requests are applied to the
  correct entity, the server must know whether the access token belongs to a
  user or an application service.  If it belongs to user, then the server musts
  know what device ID is associated with the access token, and whether the user
  is a guest.

  ## Application Services

  Application services are allowed to act on behalf of a subset of users on the
  server.  In some cases, the Application Service may have exclusive access to
  the user (that is, the user cannot be logged in in any other way), or the
  Application Service may be allowed to act on behalf of existing users.

  """

  @doc """
  Return the server name.
  """
  @spec get_server_name(server :: t()) :: String.t()
  def get_server_name(server)

  @doc """
  Return information about the user who owns the given access token.

  Must return either:

  - `{:user, user_id, device_id, guest}` if the user is a normal user, where
    `guest` is a true if the user is a guest
  - `{:application_service, user_id}` if the user is a application service
  - `nil` if the access token is not in use
  - `{:soft_logout, user_id, device_id, guest}` if the user has been
    [soft-logged out](https://spec.matrix.org/unstable/client-server-api/#soft-logout)
  """
  @spec user_from_access_token(
          server :: t(),
          access_token :: String.t()
        ) ::
          {:user, String.t(), String.t(), boolean}
          | {:application_service, String.t()}
          | nil
          | {:soft_logout, String.t(), String.t(), boolean}
  def user_from_access_token(server, access_token)

  @doc """
  Determine if the given user is in the Application Service's namespace.

  This function returns `false` if the user is not in the Application Service's
  namespace, `true` if the user is in the Application Service's namespace, but
  is not exclusive to the Application Service, and `:exclusive` if the
  Application Service has exclusive access to the user.

  A server that does not implement Application Services can make this function
  simply return `false`.

  """
  @spec user_in_appservice_namespace?(
          server :: t(),
          appservice_user_id :: String.t(),
          user_id :: String.t()
        ) :: boolean | :exclusive
  def user_in_appservice_namespace?(server, appservice_user_id, user_id)
end
