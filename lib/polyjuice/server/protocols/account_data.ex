# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.AccountData do
  @moduledoc """
  Functionality for account data.
  """

  @doc """
  Sets the user's account data.

  Should also publish to the `user_id` channel an item of the form:
  `{:account_data, stream_position, data}` when `room_id` is `nil`, and
  `{:room_account_data, stream_position, room_id, data}` otherwise.
  """
  @spec set(
          server :: Polyjuice.Server.Protocols.AccountData.t(),
          user_id :: String.t(),
          room_id :: String.t() | nil,
          type :: String.t(),
          data :: Polyjuice.Util.event_content()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def set(server, user_id, room_id, type, data)

  @doc """
  Gets the user's account data.
  """
  @spec get(
          server :: Polyjuice.Server.Protocols.AccountData.t(),
          user_id :: String.t(),
          room_id :: String.t() | nil,
          type :: String.t()
        ) ::
          {:ok, Polyjuice.Util.event_content()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get(server, user_id, room_id, type)
end
