# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.Room do
  @moduledoc """
  Functionality for interacting with rooms.
  """

  @doc """
  Create a new room.

  `state_events` is a list of `{sender, event_type, state_key, event_content}`
  that will be the initial state of the room.  The first event in the list will
  be the `m.room.create` event.

  Returns the room ID on success.

  This function should:
  - ensure that the user is allowed to create the room requested
  - calculate the necessary information for each event (e.g. prev_events,
    auth_events, event IDs, etc.)
  - store the events for future retrieval
  - notify the creator about the new room (FIXME: explain how)
  - notify any invitees that they have been invited (FIXME: explain how)

  Possible errors:
  - the user is not allowed to create the room

  """
  @spec create(
          server :: Polyjuice.Server.Protocols.Room.t(),
          state_events ::
            list({String.t(), String.t(), String.t(), Polyjuice.Util.event_content()})
        ) :: {:ok, String.t()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def create(server, state_events)

  @doc """
  Join a room locally, if possible.

  Possible `opts` are:
  - `reason`
  - `third_party_signed`

  This function should:
  - check whether the user is allowed to join the room,
  - if so, create the appropriate membership event and send it to the room
  - notify the user's devices about the initial room contents (FIXME: explain how)

  Should return `{:error, :unknown_room}` if the server does not know about the
  given room ID.  Polyjuice Server will then attempt to look up the room on a
  different server (not yet implemented).

  Possible errors:
  - the user is not allowed to join the room (`M_FORBIDDEN`)

  """
  @spec join(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          opts :: Keyword.t()
        ) :: :ok | {:error, :unknown_room} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def join(server, user_id, room_id, opts)

  @doc """
  Send an event to a room.

  Returns the event ID on success.

  The function should:
  - if `txn_id` is not `nil` check that it has not already successfully been
    used for the given device
  - check that the user is in the room and has permission to send the event
  - generate an event ID for the event
  - send the event to the room
  - notify all users/servers in the room that the event has been sent (FIXME:
    explain how)

  Possible errors:
  - the user is not allowed to send the event in the room (`M_FORBIDDEN`)
  - the user is not in the room (`M_FORBIDDEN`)
  - ...
  """
  @spec send_event(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          device_id :: String.t(),
          room_id :: String.t(),
          event_type :: String.t(),
          state_key :: String.t() | nil,
          txn_id :: String.t() | nil,
          content :: Polyjuice.Util.event_content()
        ) :: {:ok, String.t()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def send_event(server, user_id, device_id, room_id, event_type, state_key, txn_id, content)

  # FIXME: add federation version of send_event

  @doc """
  Get the current room state.

  If `event_type` is `nil`, return all state events.  If `state_key` is `nil`,
  return all state events with the given type.  If the requested state was not
  found, return the empty list, rather than an error.

  If the user is not in the room, but was previously in the room, return the
  state at the time the user left the room.

  This function should:
  - check that the user is allowed to get the state
  - return the relevant state event(s)

  Possible errors:
  - the user is not in the room, or was not in the room (`M_FORBIDDEN`)
  """
  @spec get_state(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          event_type :: String.t() | nil,
          state_key :: String.t() | nil
        ) :: {:ok, list(Polyjuice.Util.event())} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_state(server, user_id, room_id, event_type, state_key)

  @doc """
  Paginate the events in a room.

  Possible `opts` are:
  - ...
  """
  @spec get_messages(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          token :: {pos_integer, non_neg_integer} | nil,
          direction :: :forward | :backward,
          opts :: Keyword.t()
        ) ::
          {:ok, list(Polyjuice.Util.event()), String.t() | nil, list(Polyjuice.Util.event())}
          | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_messages(server, user_id, room_id, token, direction, opts)

  @spec get_event(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          event_id :: String.t()
        ) :: {:ok, Polyjuice.Util.event()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_event(server, user_id, room_id, event_id)

  # FIXME: add function for get event context

  @spec forget(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def forget(server, user_id, room_id)

  # Usually, we can just use send_event to manipulate membership, but this is
  # necessary because if we kick a banned user, we don't want to un-ban them,
  # so we can't just send a `leave` membership event
  @spec kick(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          target_user_id :: String.t(),
          reason :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def kick(server, user_id, room_id, target_user_id, reason)

  # Similarly, we only want to set an `invite` membership event if the user is
  # `leave` or `ban`
  @spec invite(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          target_user_id :: String.t(),
          reason :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def invite(server, user_id, room_id, target_user_id, reason)

  # And unbanning a user who is already in the room shouldn't kick them
  @spec unban(
          server :: Polyjuice.Server.Protocols.Room.t(),
          user_id :: String.t(),
          room_id :: String.t(),
          target_user_id :: String.t(),
          reason :: String.t()
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def unban(server, user_id, room_id, target_user_id, reason)

  # FIXME: knocking

  # FIXME: room aliases?
end
