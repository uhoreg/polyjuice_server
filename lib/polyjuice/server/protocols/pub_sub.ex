# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.PubSub do
  @moduledoc """
  Publish-subscribe functionality.

  Any server that will handle events must implement this protocol.  This allows
  subscribers to be notified of events when they happen, and is the mechanism
  used by the client `/sync` endpoint, sending events over federation, and
  notifying application services.

  FIXME: more details
  """

  @spec publish(server :: t(), data :: any, queues :: list()) :: any
  def publish(server, data, queues)

  @spec subscribe(
          server :: t(),
          subscriber :: Polyjuice.Server.Protocols.Subscriber.t(),
          queues :: list()
        ) :: any
  def subscribe(server, subscriber, queues)

  @spec unsubscribe(
          server :: t(),
          subscriber :: Polyjuice.Server.Protocols.Subscriber.t(),
          queues :: list()
        ) :: any
  def unsubscribe(server, subscriber, queues)

  @spec await_sync(
          server :: t(),
          user_id :: String.t(),
          device_id :: String.t(),
          sync_token :: Polyjuice.Server.SyncToken.t() | nil,
          timeout :: non_neg_integer,
          filter :: map()
        ) :: {Polyjuice.Server.SyncToken.t() | nil, map()} | :logout | :soft_logout
  def await_sync(server, user_id, device_id, sync_token, timeout, filter)

  @spec get_sync_data_since(
          server :: t(),
          user_id :: String.t(),
          device_id :: String.t(),
          sync_token :: Polyjuice.Server.SyncToken.t() | nil,
          filter :: map()
        ) :: {Polyjuice.Server.SyncToken.t() | nil, map()} | :logout | :soft_logout
  def get_sync_data_since(server, user_id, device_id, sync_token, filter)

  @spec add_filter(
          server :: t(),
          user_id :: String.t(),
          filter :: map
        ) :: String.t()
  def add_filter(server, user_id, filter)

  @spec get_filter(
          server :: t(),
          user_id :: String.t(),
          filter_id :: String.t()
        ) :: {:ok, map()} | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def get_filter(server, user_id, filter_id)
end
