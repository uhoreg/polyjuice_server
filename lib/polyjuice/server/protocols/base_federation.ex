# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.BaseFederation do
  @moduledoc """
  Base federation functionality.

  Any server that handles federation requests must implement this protocol.

  Note: servers will generally also need to implement the
  `Polyuice.Server.Federation.Client.API` protocol so that it can get other
  servers' signing keys to authenticate their requests.  If it is not
  implemented then only servers whose keys are cached (`get_cached_remote_keys`)
  will be able to make authenticated requests.

  ## Server keys

  Servers use signing keys to authenticate requests and events; other servers
  can retrieve the server's public key to verify these.  Servers must also make
  sure that the public part of old signing keys is available so that other
  servers can verify old events.  Each key is identified by an ID that is
  prefixed by the signing algorithm (typically "ed25519" since this is the only
  currently-supported algorithm).  For example, if a server has a key `foo`,
  then the prefixed ID would be `ed25519:foo`.

  A keypair can be created by doing:

      {pub, priv} = :crypto.generate_key(:eddsa, :ed25519)
      {Base.encode64(pub, padding: false),
       Polyjuice.Util.Ed25519.SigningKey.from_base64(Base.encode64(priv, padding: false), "1")}

  """

  @doc """
  Return the server name.
  """
  @spec get_server_name(server :: Polyjuice.Server.Protocols.BaseFederation.t()) :: String.t()
  def get_server_name(server)

  @doc """
  Get the server's current signing keys.

  Returns a map from prefixed key IDs (in the form "algorithm:id",
  e.g. "ed25519:1") to `{Polyjuice.Util.SigningKey.t(), %{key: <base64-encoded
  public key>}, <valid_until>}`, where `valid_until` is the time until which
  the key should be considered valid, as a millisecond timestamp that must be
  in the future, or `nil` if the key does not have a set expiry time.  The map
  will usually contain only one key.

  """
  @spec get_keys(server :: Polyjuice.Server.Protocols.BaseFederation.t()) :: %{
          required(String.t()) => {Polyjuice.Util.SigningKey, %{key: String.t()}, integer}
        }
  def get_keys(server)

  @doc """
  Get the server's old signing keys.

  Returns a map from prefixed key IDs (in the form "algorithm:id",
  e.g. "ed25519:1") to `%{key: <base64-encoded public key>, expired_ts:
  <timestamp>}`, where `timestamp` is in milliseconds.

  """
  @spec get_old_keys(server :: Polyjuice.Server.Protocols.BaseFederation.t()) :: %{
          optional(String.t()) => %{
            key: String,
            expired_ts: integer
          }
        }
  def get_old_keys(server)

  @doc """
  Get a cached version of the remote server's signing key.

  `key_ids` is a list of key IDs to return, or `nil` to return all of the
  server's keys.

  Returns a map from key ID to a tuple of the form
  `{Polyjuice.Util.VerifyKey.t(), <valid_until>, <expired>}`, where
  `valid_until` is the time until which the key should be considered valid, as
  a millisecond timestamp.

  """
  @spec get_cached_remote_keys(
          server :: Polyjuice.Server.Protocols.BaseFederation.t(),
          origin :: String.t(),
          key_ids :: list(String.t()) | nil
        ) :: %{optional(String.t()) => {Polyjuice.Util.VerifyKey.t(), integer, boolean}}
  def get_cached_remote_keys(server, origin, key_ids \\ nil)

  @doc """
  Cache a remote server's signing keys.

  The `keys` parameter is a map from key ID to a tuple of the form
  `{Polyjuice.Util.VerifyKey.t(), <valid_until>, <expired>}`, thus is in a form
  where `get_cached_remote_keys` can simply return this map, limited to the
  requested key IDs.

  """
  @spec cache_remote_keys(
          server :: Polyjuice.Server.Protocols.BaseFederation.t(),
          origin :: String.t(),
          keys :: %{required(String.t()) => {Polyjuice.Util.VerifyKey.t(), integer, boolean}}
        ) :: :ok
  def cache_remote_keys(server, origin, keys)
end
