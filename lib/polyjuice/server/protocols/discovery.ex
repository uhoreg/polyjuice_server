# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defprotocol Polyjuice.Server.Protocols.Discovery do
  @moduledoc """
  Server discovery allows a server's address to be found from the server name.

  This module is used to implement server discovery using `.well-known` files.

  """

  @doc """
  Return the server's base URL.
  """
  @spec address(server :: Polyjuice.Server.Protocols.Discovery.t()) :: String.t() | URI.t() | nil
  def address(server)

  @doc """
  Return the identity server's base URL.
  """
  @spec identity_server(server :: Polyjuice.Server.Protocols.Discovery.t()) ::
          String.t() | URI.t() | nil
  def identity_server(server)

  @doc """
  Extra fields to include in the `.well-known` file for clients.
  """
  @spec extra_fields(server :: Polyjuice.Server.Protocols.Discovery.t()) :: map() | nil
  def extra_fields(server)

  @doc """
  Return the delegated hostname.

  The delegated hostname can be used to discover the actual server address for
  federation (via an SRV DNS record) or can be the server address itself.

  """
  @spec delegated_hostname(server :: Polyjuice.Server.Protocols.Discovery.t()) ::
          URI.t() | String.t() | nil
  def delegated_hostname(server)
end
