# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Protocols.Helper.BaseFederation do
  alias Polyjuice.Server.Protocols.Helper

  @callback get_server_name(args :: {}, from :: GenServer.from(), state :: term) ::
              Helper.call_result(String.t())

  @callback get_keys(args :: {}, from :: GenServer.from(), state :: term) ::
              Helper.call_result(%{
                required(String.t()) => {Polyjuice.Util.SigningKey, %{key: String.t()}, integer}
              })

  @callback get_old_keys(args :: {}, from :: GenServer.from(), state :: term) ::
              Helper.call_result(%{
                optional(String.t()) => %{
                  key: String,
                  expired_ts: integer
                }
              })

  @callback get_cached_remote_keys(
              args :: {String.t(), list(String.t()) | nil},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(%{
                optional(String.t()) => {Polyjuice.Util.VerifyKey.t(), integer, boolean}
              })

  @callback cache_remote_keys(
              args ::
                {String.t(),
                 %{required(String.t()) => {Polyjuice.Util.VerifyKey.t(), integer, boolean}}},
              from :: GenServer.from(),
              state :: term
            ) ::
              Helper.call_result(:ok)
end
