# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.SyncHelper do
  @moduledoc """
  Helps manage `/sync` calls.

  This module manages a set of worker processes.  When a sync call is made (and
  we don't have a worker process already waiting), we start a new worker
  process, which will first check for waiting events and if none are found,
  listen for incoming events.  When the sync call returns, the worker sticks
  around, waiting for the next call on the next sync token.  While it waits, it
  receives events so that it doesn't need to start from scratch on the next
  sync token -- it will already know whether there are events waiting.

  """

  @type state() :: %{
          server: Polyjuice.Server.Protocols.PubSub.t() | nil,
          # user ID -> device ID -> {active_listeners, %{token -> waiting_listeners}}
          workers: %{
            optional(String.t()) => %{
              required(String.t()) =>
                {MapSet.t(pid), %{optional(Polyjuice.Server.SyncToken.t()) => MapSet.t(pid)}}
            }
          },
          # pid -> {user_id, device_id, sync_token | nil, from | nil}
          worker_info: %{
            optional(pid) =>
              {String.t(), String.t(), Polyjuice.Server.SyncToken.t() | nil,
               GenServer.from() | nil}
          }
        }

  use GenServer

  @impl GenServer
  def init([]) do
    {:ok, %{server: nil, workers: %{}, worker_info: %{}}}
  end

  def sync(pid, server, user_id, device_id, sync_token, timeout, filter)
      when is_binary(user_id) and is_binary(device_id) and is_integer(timeout) and timeout >= 0 and
             is_map(filter) do
    GenServer.call(
      pid,
      {:sync, server, user_id, device_id, sync_token, timeout, filter},
      timeout + 5000
    )
  end

  @impl GenServer
  def handle_call({:sync, server, user_id, device_id, sync_token, timeout, filter}, from, state) do
    {new_workers, new_worker_info} =
      case Map.fetch(state.workers, user_id) do
        {:ok, devices} ->
          {new_devices, new_worker_info} =
            case Map.fetch(devices, device_id) do
              {:ok, {active, tokens}} ->
                {new_device_info, new_worker_info} =
                  case Map.fetch(tokens, sync_token) do
                    {:ok, waiting} ->
                      # use an existing worker
                      [pid] = Enum.take(waiting, 1)
                      GenServer.cast(pid, {:sync, from, timeout, filter})

                      new_tokens =
                        if MapSet.size(waiting) == 1 do
                          Map.delete(tokens, sync_token)
                        else
                          Map.put(tokens, sync_token, MapSet.delete(waiting, pid))
                        end

                      new_active = MapSet.put(active, pid)

                      {{new_active, new_tokens},
                       Map.put(
                         state.worker_info,
                         pid,
                         {user_id, device_id, sync_token, from}
                       )}

                    :error ->
                      # create a new worker
                      {:ok, pid} =
                        GenServer.start(Polyjuice.Server.SyncHelper.Worker, [
                          self(),
                          server,
                          user_id,
                          device_id,
                          from,
                          sync_token,
                          timeout,
                          filter
                        ])

                      new_active = MapSet.put(active, pid)

                      {{new_active, tokens},
                       Map.put(
                         state.worker_info,
                         pid,
                         {user_id, device_id, sync_token, from}
                       )}
                  end

                {Map.put(devices, device_id, new_device_info), new_worker_info}

              :error ->
                # subscribe and create a new worker
                Polyjuice.Server.Protocols.PubSub.subscribe(server, self(), [{user_id, device_id}])

                {:ok, pid} =
                  GenServer.start(Polyjuice.Server.SyncHelper.Worker, [
                    self(),
                    server,
                    user_id,
                    device_id,
                    from,
                    sync_token,
                    timeout,
                    filter
                  ])

                {Map.put(devices, device_id, {MapSet.new([pid]), %{}}),
                 Map.put(
                   state.worker_info,
                   pid,
                   {user_id, device_id, sync_token, from}
                 )}
            end

          {Map.put(state.workers, user_id, new_devices), new_worker_info}

        :error ->
          Polyjuice.Server.Protocols.PubSub.subscribe(server, self(), [
            user_id,
            {user_id, device_id}
          ])

          {:ok, pid} =
            GenServer.start(Polyjuice.Server.SyncHelper.Worker, [
              self(),
              server,
              user_id,
              device_id,
              from,
              sync_token,
              timeout,
              filter
            ])

          {Map.put(state.workers, user_id, %{device_id => {MapSet.new([pid]), %{}}}),
           Map.put(
             state.worker_info,
             pid,
             {user_id, device_id, sync_token, from}
           )}
      end

    {:noreply, %{state | server: server, workers: new_workers, worker_info: new_worker_info}}
  end

  @impl GenServer
  def handle_cast({:sent_sync, pid, next_token}, state) do
    # move worker from active to waiting
    {user_id, device_id, _, _} = Map.get(state.worker_info, pid)
    devices = Map.fetch!(state.workers, user_id)
    {active, tokens} = Map.fetch!(devices, device_id)
    new_active = MapSet.delete(active, pid)

    new_tokens =
      Map.update(
        tokens,
        next_token,
        MapSet.new([pid]),
        &MapSet.put(&1, pid)
      )

    new_devices = Map.put(devices, device_id, {new_active, new_tokens})
    new_workers = Map.put(state.workers, user_id, new_devices)
    new_worker_info = Map.put(state.worker_info, pid, {user_id, device_id, next_token, nil})
    {:noreply, %{state | workers: new_workers, worker_info: new_worker_info}}
  end

  def handle_cast(_, state) do
    {:noreply, state}
  end

  @impl GenServer
  def handle_info({:notify, user_id, data}, state) when is_binary(user_id) do
    # calculate new sync token, get all the listeners in
    # state.listeners[user_id][*], poke them, update state to new sync token
    case data do
      :logout_all ->
        Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [user_id])

        devices = Map.fetch!(state.workers, user_id)

        new_worker_info =
          Enum.reduce(
            devices,
            state.worker_info,
            fn {device_id, {active, tokens}}, acc ->
              Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [
                {user_id, device_id}
              ])

              Enum.each(active, &GenServer.cast(&1, :logout))
              new_worker_info = Map.drop(acc, MapSet.to_list(active))

              Enum.reduce(tokens, new_worker_info, fn {_, waiting}, acc ->
                Enum.each(waiting, &GenServer.cast(&1, :logout))
                Map.drop(acc, MapSet.to_list(waiting))
              end)
            end
          )

        new_workers = Map.delete(state.workers, user_id)
        {:noreply, %{state | workers: new_workers, worker_info: new_worker_info}}

      x ->
        devices = Map.fetch!(state.workers, user_id)

        Enum.each(
          devices,
          fn {_, {active, tokens}} ->
            Enum.each(active, &GenServer.cast(&1, {:notify, x}))

            Enum.each(tokens, fn {_, waiting} ->
              Enum.each(waiting, &GenServer.cast(&1, {:notify, x}))
            end)
          end
        )

        {:noreply, state}
    end
  end

  def handle_info({:notify, {user_id, device_id}, data}, state) when is_binary(user_id) do
    # calculate new sync token, get all the workers in
    # state.workers[user_id][device_id], poke them, update state to new sync
    # token
    case data do
      :logout ->
        Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [{user_id, device_id}])

        devices = Map.fetch!(state.workers, user_id)
        {active, tokens} = Map.fetch!(devices, device_id)
        Enum.each(active, &GenServer.cast(&1, :logout))
        new_worker_info = Map.drop(state.worker_info, MapSet.to_list(active))

        new_worker_info =
          Enum.reduce(tokens, new_worker_info, fn {_, waiting}, acc ->
            Enum.each(waiting, &GenServer.cast(&1, :logout))
            Map.drop(acc, MapSet.to_list(waiting))
          end)

        new_devices = Map.delete(devices, device_id)

        new_workers =
          if new_devices == %{} do
            Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [user_id])
            Map.delete(state.workers, user_id)
          else
            Map.put(state.workers, user_id, new_devices)
          end

        {:noreply, %{state | workers: new_workers, worker_info: new_worker_info}}

      x ->
        devices = Map.fetch!(state.workers, user_id)
        {active, tokens} = Map.fetch!(devices, device_id)
        Enum.each(active, &GenServer.cast(&1, {:notify, x}))

        Enum.each(tokens, fn {_, waiting} ->
          Enum.each(waiting, &GenServer.cast(&1, {:notify, x}))
        end)

        {:noreply, state}
    end
  end

  def handle_info({:DOWN, _, :process, pid, _}, state) do
    # one of our children went down.  If it was serving a sync request, return
    # an empty response. FIXME: or start a new process?  And update our
    # bookkeeping.
    new_workers =
      case Map.get(state.worker_info, pid) do
        {user_id, device_id, token, nil} ->
          # the process was waiting
          with {:ok, devices} <- Map.fetch(state.workers, user_id),
               {:ok, {active, tokens}} <- Map.fetch(devices, device_id),
               {:ok, waiting} <- Map.fetch(tokens, token) do
            new_waiting = MapSet.delete(waiting, pid)

            new_tokens =
              if MapSet.size(new_waiting) == 0 do
                Map.delete(tokens, token)
              else
                Map.put(tokens, token, new_waiting)
              end

            new_devices =
              if MapSet.size(active) == 0 and new_tokens == %{} do
                Map.delete(devices, device_id)

                Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [
                  {user_id, device_id}
                ])
              else
                Map.put(devices, device_id, {active, new_tokens})
              end

            if new_devices == %{} do
              Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [user_id])
              Map.delete(state.workers, user_id)
            else
              Map.put(state.workers, user_id, new_devices)
            end
          else
            _ -> state.workers
          end

        {user_id, device_id, token, from} ->
          # the process was active
          GenServer.reply(from, {token, %{}})

          with {:ok, devices} <- Map.fetch(state.workers, user_id),
               {:ok, {active, tokens}} <- Map.fetch(devices, device_id) do
            new_active = MapSet.delete(active, pid)

            new_devices =
              if MapSet.size(new_active) == 0 and tokens == %{} do
                Map.delete(devices, device_id)

                Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [
                  {user_id, device_id}
                ])
              else
                Map.put(devices, device_id, {new_active, tokens})
              end

            if new_devices == %{} do
              Polyjuice.Server.Protocols.PubSub.unsubscribe(state.server, self(), [user_id])
              Map.delete(state.workers, user_id)
            else
              Map.put(state.workers, user_id, new_devices)
            end
          else
            _ -> state.workers
          end

        nil ->
          state.workers
      end

    new_worker_info = Map.delete(state.worker_info, pid)
    {:noreply, %{state | workers: new_workers, worker_info: new_worker_info}}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  # FIXME: kill workers and unsubscribe from everything when we terminate

  defmodule Worker do
    @moduledoc false

    use GenServer

    @typedoc false
    @type state() :: %{
            parent: pid,
            server: Polyjuice.Server.Protocols.PubSub.t(),
            user_id: String.t(),
            device_id: String.t(),
            # the sync token that the worker is processing
            sync_token: Polyjuice.Server.SyncToken.t() | nil,
            # the sync token that the worker will send if it responds right away
            next_token: Polyjuice.Server.SyncToken.t() | nil,
            # accumulated data to send to the client (after processing and
            # filtering)  Format:
            # %{
            #   account_data: list of account data, most recent first
            #   rooms: %{
            #     room_id => %{
            #       events: [{stream_pos, list of events} ... ] (most recent first)
            #       account_data: list of account data, most recent first
            #       ...
            #     }
            #   }
            #   to_device: list of events, most recent first
            #   ....
            # }
            data: map(),
            # process to notify with data (with GenServer.reply), or nil if no
            # process is waiting
            from: GenServer.from() | nil,
            filter: map() | nil,
            soft_logout: boolean
          }

    defp has_data?(data, filter) do
      # FIXME:
      data != %{}
    end

    # These functions process a list of {position, events} based on the given
    # limit, and produce data that can be used to generate the sync response.
    defp do_split_before([], _, timeline_events, token, join_state, _) do
      {Enum.concat(timeline_events), [], false, token, join_state}
    end

    defp do_split_before(
           [{pos, events} | rest],
           limit_remaining,
           timeline_events,
           _,
           join_state,
           user_id
         ) do
      join_state =
        if join_state do
          join_state
        else
          Enum.reduce(
            events,
            nil,
            fn
              %{
                "type" => "m.room.member",
                "state_key" => ^user_id,
                "content" => %{"membership" => "join"}
              },
              _ ->
                "join"

              %{
                "type" => "m.room.member",
                "state_key" => ^user_id,
                "content" => %{"membership" => _}
              },
              _ ->
                "leave"

              _, join_state ->
                join_state
            end
          )
        end

      length = Enum.count(events)

      if length > limit_remaining do
        {events_before, events_after} = Enum.split(events, length - limit_remaining)
        state = Enum.filter(events_before, &Map.has_key?(&1, "state_key"))

        # FIXME: any state events that appear immediately after, we should
        # include in the timeline anyways (as long as it's the first of that
        # type/state_key), because they'll be included in the state section
        # anyways, and we can make the client do less backfilling.
        do_split_after(
          rest,
          [events_after | timeline_events],
          [state],
          "r#{pos}.#{length - limit_remaining}",
          join_state,
          user_id
        )
      else
        do_split_before(
          rest,
          limit_remaining - length,
          [events | timeline_events],
          "r#{pos}.0",
          join_state,
          user_id
        )
      end
    end

    defp do_split_after([], timeline_events, state, token, join_state, _) do
      state =
        Enum.concat(state)
        |> Enum.reverse()
        |> Enum.uniq_by(fn %{"type" => type, "state_key" => state_key} -> {type, state_key} end)
        |> Enum.reverse()

      {
        Enum.concat(timeline_events),
        state,
        true,
        token,
        join_state
      }
    end

    defp do_split_after([{_, events} | rest], timeline_events, state, token, join_state, user_id) do
      join_state =
        if join_state do
          join_state
        else
          Enum.reduce(
            events,
            nil,
            fn
              %{
                "type" => "m.room.member",
                "state_key" => ^user_id,
                "content" => %{"membership" => "join"}
              },
              _ ->
                "join"

              %{
                "type" => "m.room.member",
                "state_key" => ^user_id,
                "content" => %{"membership" => _}
              },
              _ ->
                "leave"

              _, join_state ->
                join_state
            end
          )
        end

      new_state = Enum.filter(events, &Map.has_key?(&1, "state_key"))
      do_split_after(rest, timeline_events, [new_state | state], token, join_state, user_id)
    end

    # FIXME: apply filter
    # FIXME: move this to a utility module, since it is useful elsewhere
    def split_events(events, limit, user_id) do
      do_split_before(events, limit, [], nil, nil, user_id)
    end

    defp send_data(state, data \\ nil) do
      # FIXME: apply filter to data

      user_id = state.user_id

      data =
        if data != nil do
          # don't do anything if the data to send was specified
          data
        else
          data = %{}

          # account data
          data =
            case state.data do
              %{account_data: account_data} ->
                Map.put(data, "account_data", %{"events" => Enum.reverse(account_data)})

              _ ->
                data
            end

          # device lists
          data =
            case state.data do
              %{device_keys_changed: changed_users} ->
                Map.put(data, "device_lists", %{"changed" => changed_users})

              _ ->
                data
            end

          # rooms
          data =
            case state.data do
              %{rooms: rooms} ->
                Enum.reduce(rooms, data, fn {room_id, room_info}, acc ->
                  room = %{}

                  {join_state, room} =
                    case room_info do
                      %{events: events} ->
                        # FIXME: limit
                        {timeline_events, room_state, limited, prev_token, join_state} =
                          split_events(events, 10, user_id)

                        timeline_events =
                          Enum.map(
                            timeline_events,
                            &Polyjuice.Server.Room.to_client_event(&1, user_id, state.device_id)
                          )

                        room_state =
                          Enum.map(
                            room_state,
                            &Polyjuice.Server.Room.to_client_event(&1, user_id, state.device_id)
                          )

                        {join_state || "join",
                         Map.merge(
                           room,
                           %{
                             # FIXME: drop empty properties
                             "timeline" => %{
                               "events" => timeline_events,
                               "limited" => limited,
                               "prev_batch" => prev_token
                             },
                             "state" => %{
                               "events" => room_state
                             }
                           }
                         )}

                      _ ->
                        {nil, room}
                    end

                  rooms = Map.get(acc, "rooms", %{})

                  rooms_for_join_state =
                    Map.get(rooms, join_state, %{})
                    |> Map.put(room_id, room)

                  new_rooms = Map.put(rooms, join_state, rooms_for_join_state)
                  Map.put(acc, "rooms", new_rooms)
                end)

              _ ->
                data
            end

          data =
            case state.data do
              %{to_device: events} ->
                Map.put(data, "to_device", %{"events" => Enum.reverse(events)})

              _ ->
                data
            end

          data
        end

      # add the key counts
      data =
        if Polyjuice.Server.Protocols.DeviceKey.impl_for(state.server) == nil do
          data
        else
          key_counts =
            Polyjuice.Server.Protocols.DeviceKey.key_counts(
              state.server,
              state.user_id,
              state.device_id
            )

          Map.put(data, "device_one_time_keys_count", key_counts.one_time_keys)
        end

      GenServer.cast(state.parent, {:sent_sync, self(), state.next_token})
      GenServer.reply(state.from, {state.next_token, data})

      {:noreply,
       %{
         state
         | sync_token: state.next_token,
           data: %{},
           from: nil,
           filter: nil,
           timeout_time: :erlang.monotonic_time(:millisecond) + 20_000
       }, 20_000}
    end

    def mark_send_to_device_received(state) do
      if Polyjuice.Server.Protocols.SendToDevice.impl_for(state.server) do
        Polyjuice.Server.Protocols.SendToDevice.mark_received(
          state.server,
          state.user_id,
          state.device_id,
          Polyjuice.Server.SyncToken.get_component(state.sync_token, :d)
        )
      end
    end

    @impl GenServer
    def init([parent, server, user_id, device_id, from, sync_token, timeout, filter]) do
      {:ok,
       %{
         parent: parent,
         server: server,
         user_id: user_id,
         device_id: device_id,
         from: from,
         sync_token: sync_token,
         filter: filter,
         data: %{},
         next_token: sync_token,
         soft_logout: false,
         timeout_time: :erlang.monotonic_time(:millisecond) + timeout
       }, {:continue, nil}}
    end

    @impl GenServer
    def handle_continue(_, state) do
      mark_send_to_device_received(state)

      case Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
             state.server,
             state.user_id,
             state.device_id,
             state.sync_token,
             state.filter
           ) do
        # FIXME:
        :logout ->
          GenServer.reply(state.from, :logout)
          {:stop, :normal, nil}

        :soft_logout ->
          GenServer.cast(state.parent, {:sent_sync, self(), state.sync_token})
          GenServer.reply(state.from, :soft_logout)

          {:noreply,
           %{
             state
             | from: nil,
               filter: nil,
               soft_logout: true,
               timeout_time: :erlang.monotonic_time(:millisecond) + 20_000
           }, 20_000}

        {next_token, data} when data == %{} ->
          timeout = state.timeout_time - :erlang.monotonic_time(:millisecond)
          next_state = %{state | next_token: next_token}

          if timeout <= 0 do
            send_data(next_state)
          else
            {:noreply, next_state, timeout}
          end

        {next_token, data} ->
          send_data(%{state | next_token: next_token}, data)
      end
    end

    @impl GenServer
    def handle_cast({:sync, from, timeout, filter}, state) do
      mark_send_to_device_received(state)

      if state.soft_logout do
        GenServer.cast(state.parent, {:sent_sync, self(), state.sync_token})
        GenServer.reply(from, :soft_logout)

        {:noreply,
         %{
           state
           | timeout_time: :erlang.monotonic_time(:millisecond) + 20_000
         }, 20_000}
      else
        if has_data?(state.data, filter) do
          send_data(%{state | from: from, filter: filter})
        else
          timeout_time = :erlang.monotonic_time(:millisecond) + timeout
          {:noreply, %{state | from: from, filter: filter, timeout_time: timeout_time}, timeout}
        end
      end
    end

    def handle_cast(:logout, state) do
      if state.from do
        GenServer.reply(state.from, :logout)
      end

      {:stop, :normal, nil}
    end

    def handle_cast({:notify, :soft_logout}, state) do
      if state.from do
        GenServer.cast(state.parent, {:sent_sync, self(), state.sync_token})
        GenServer.reply(state.from, :soft_logout)

        {:noreply,
         %{
           state
           | from: nil,
             filter: nil,
             soft_logout: true,
             timeout_time: :erlang.monotonic_time(20_000)
         }, 20_000}
      else
        timeout = state.timeout_time - :erlang.monotonic_time(:millisecond)

        if timeout <= 0 do
          {:stop, :normal, nil}
        else
          {:noreply, %{state | soft_logout: true}, timeout}
        end
      end
    end

    def handle_cast({:notify, :login}, state) do
      timeout = state.timeout_time - :erlang.monotonic_time(:millisecond)

      if timeout <= 0 do
        {:stop, :normal, nil}
      else
        {:noreply, %{state | from: nil, filter: nil, soft_logout: false}, timeout}
      end
    end

    def handle_cast({:notify, data}, state) do
      {new_state, notify} =
        case data do
          # account data
          {:account_data, pos, data} ->
            if Polyjuice.Server.SyncToken.is_newer(state.next_token, :a, pos) do
              events = Map.get(state.data, :account_data, [])
              new_data = Map.put(state.data, :account_data, [data | events])

              new_next_token = Polyjuice.Server.SyncToken.update(state.next_token, :a, pos)
              # FIXME: check if account data is filtered out
              {%{state | data: new_data, next_token: new_next_token}, true}
            else
              {state, false}
            end

          # FIXME: room account data too

          # device keys
          {:device_keys_changed, pos, user_id} ->
            if Polyjuice.Server.SyncToken.is_newer(state.next_token, :k, pos) do
              new_next_token = Polyjuice.Server.SyncToken.update(state.next_token, :k, pos)

              {%{
                 state
                 | data: Map.update(state.data, :device_keys_changed, [user_id], &[user_id | &1]),
                   next_token: new_next_token
               }, true}
            else
              {state, false}
            end

          # room
          {:room_events, pos, room_id, events} ->
            if Polyjuice.Server.SyncToken.is_newer(state.next_token, :r, pos) do
              rooms = Map.get(state.data, :rooms, %{})
              room = Map.get(rooms, room_id, %{})
              room_events = Map.get(room, :events, [])
              new_room_events = [{pos, events} | room_events]
              new_room = Map.put(room, :events, new_room_events)
              new_rooms = Map.put(rooms, room_id, new_room)
              new_data = Map.put(state.data, :rooms, new_rooms)

              new_next_token = Polyjuice.Server.SyncToken.update(state.next_token, :r, pos)
              # FIXME: check if events are filtered out
              {%{state | data: new_data, next_token: new_next_token}, true}
            else
              {state, false}
            end

          {:invite, pos, room_id, stripped_state} ->
            if Polyjuice.Server.SyncToken.is_newer(state.next_token, :r, pos) do
              invites =
                Map.get(state.data, :invites, %{})
                |> Map.put(room_id, stripped_state)

              new_data = Map.put(state.data, :invites, invites)

              new_next_token = Polyjuice.Server.SyncToken.update(state.next_token, :r, pos)
              {%{state | data: new_data, next_token: new_next_token}, true}
            else
              {state, false}
            end

          # send to device
          {:to_device, pos, event} ->
            if Polyjuice.Server.SyncToken.is_newer(state.next_token, :d, pos) do
              events = Map.get(state.data, :to_device, [])
              new_data = Map.put(state.data, :to_device, [event | events])

              new_next_token = Polyjuice.Server.SyncToken.update(state.next_token, :d, pos)
              {%{state | data: new_data, next_token: new_next_token}, true}
            else
              {state, false}
            end

            # FIXME: more events...
        end

      if state.from do
        if notify do
          send_data(new_state)
        else
          timeout = state.timeout_time - :erlang.monotonic_time(:millisecond)

          if timeout <= 0 do
            send_data(new_state)
          else
            {:noreply, new_state, timeout}
          end
        end
      else
        timeout = state.timeout_time - :erlang.monotonic_time(:millisecond)

        if timeout <= 0 do
          {:stop, :normal, nil}
        else
          {:noreply, new_state, timeout}
        end
      end
    end

    @impl GenServer
    def handle_info(:timeout, state) do
      if state.from do
        send_data(state)
      else
        {:stop, :normal, nil}
      end
    end

    def handle_info(_, state) do
      {:noreply, state}
    end
  end
end
