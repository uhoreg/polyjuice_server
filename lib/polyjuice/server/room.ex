# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Room do
  @doc """
  Return the stripped version of the current room state.

  https://spec.matrix.org/unstable/client-server-api/#stripped-state
  """
  @spec strip_state(%{required({String.t(), String.t()}) => Polyjuice.Util.event()}) ::
          list(Polyjuice.Util.event())
  def strip_state(room_state) do
    Map.take(
      room_state,
      [
        {"m.room.create", ""},
        {"m.room.name", ""},
        {"m.room.avatar", ""},
        {"m.room.topic", ""},
        {"m.room.join_rules", ""},
        {"m.room.canonical_alias", ""},
        {"m.room.encryption", ""}
      ]
    )
    |> Enum.map(fn {_type, event} ->
      Map.take(event, ["content", "sender", "state_key", "type"])
    end)
  end

  @doc false
  # convert an event from server format to client format
  @spec to_client_event(Polyjuice.Util.event(), String.t(), String.t()) :: Polyjuice.Util.event()
  def to_client_event(event, user_id, device_id) do
    event_sender_device = Map.get(event, :sender_device, :none)

    event = Map.take(event, ~w(
        content
        event_id
        origin_server_ts
        room_id
        sender
        state_key
        type
        unsigned
      ))

    event =
      case event do
        %{"origin_server_ts" => ts} ->
          age = :erlang.system_time(:millisecond) - ts
          Map.update(event, "unsigned", %{"age" => age}, &Map.put(&1, "age", age))

        _ ->
          event
      end

    event =
      with true <- {user_id, device_id} != {Map.get(event, "sender"), event_sender_device},
           %{"unsigned" => unsigned} <- event do
        Map.put(event, "unsigned", Map.delete(unsigned, "transaction_id"))
      else
        _ ->
          event
      end

    event
  end

  defprotocol Stream do
    @moduledoc """
    A stream of events in a room
    """

    @spec split(t(), map()) :: {list(Polyjuice.Util.event()), list(Polyjuice.Util.event())}
    def split(stream, filter)

    @spec take(t(), map()) :: list(Polyjuice.Util.event())
    def take(stream, filter)
  end
end
