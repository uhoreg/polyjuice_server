# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.Federation.Discover do
  # Cache for the well-known results
  defprotocol Cache do
    # Get the cached value (if any).  Returns the cached value (which will be
    # of the form `{:ok, json}` or `{:error, term}`), or `nil` if there is no
    # cached value or if it expired.
    def get(cache, server_name)
    # Cache the value for the server_name.  The value should only be cached for
    # `lifetime` seconds.
    def put(cache, server_name, value, lifetime)
  end

  # to use `nil` as cache (i.e. no cache)
  defimpl Cache, for: Atom do
    def get(_, _) do
      nil
    end

    def put(_, _, _, _) do
      nil
    end
  end

  @type discovery() :: %{
          locations: list({String.t(), integer}),
          host: String.t(),
          identity: String.t()
        }

  @doc """
  Discover the address of a Matrix server.

  Returns a list of {hostname, port} to try to contact the server at (locations
  should be tried in the order given), the name to use in the `Host` header,
  and the identity that should be included in the server's certificate.

  Examples:

      iex> Polyjuice.Server.Federation.Discover.discover("1.2.3.4:1234", nil)
      %{locations: [{"1.2.3.4", 1234}], host: "1.2.3.4:1234", identity: "1.2.3.4"}
      iex> Polyjuice.Server.Federation.Discover.discover("1.2.3.4", nil)
      %{locations: [{"1.2.3.4", 8448}], host: "1.2.3.4", identity: "1.2.3.4"}
      iex> Polyjuice.Server.Federation.Discover.discover("test.uhoreg.ca:1234", nil)
      %{locations: [{"test.uhoreg.ca", 1234}], host: "test.uhoreg.ca:1234", identity: "test.uhoreg.ca"}

  """
  @spec discover(server_name :: String.t(), cache :: Polyjuice.Server.Federation.Discover.Cache) ::
          discovery() | nil
  def discover(server_name, cache) when is_binary(server_name) do
    as_ip_literal(server_name) || as_hostname_with_port(server_name) ||
      via_wellknown(server_name, cache) || via_srv(server_name) ||
      %{locations: [{server_name, 8448}], host: server_name, identity: server_name}

    # 5. If the /.well-known request returned an error response, and the SRV
    # record was not found, an IP address is resolved using AAAA and A
    # records. Requests are made to the resolved IP address using port 8448 and
    # a Host header containing the <hostname>. The target server must present a
    # valid certificate for <hostname>.
  end

  defp as_ip_literal(server_name) do
    # 1. If the hostname is an IP literal, then that IP address should be used,
    # together with the given port number, or 8448 if no port is given. The
    # target server must present a valid certificate for the IP address. The
    # Host header in the request should be set to the server name, including
    # the port if the server name included one.
    case Regex.run(
           ~r/(\d+\.\d+\.\d+\.\d+|\[[0-9a-fA-F:]+(?::\d+\.\d+\.\d+\.\d+)?\])(?::(\d+))?/,
           server_name
         ) do
      [_, addr] ->
        %{locations: [{addr, 8448}], host: addr, identity: addr}

      [_, addr, port] ->
        %{locations: [{addr, String.to_integer(port)}], host: server_name, identity: addr}

      _ ->
        nil
    end
  end

  defp as_hostname_with_port(server_name) do
    # 2. If the hostname is not an IP literal, and the server name includes an
    # explicit port, resolve the IP address using AAAA or A records. Requests
    # are made to the resolved IP address and given port with a Host header of
    # the original server name (with port). The target server must present a
    # valid certificate for the hostname.
    case Regex.run(~r/([-a-z]+(?:\.[-a-z]+)*):(\d+)/, server_name) do
      [_, hostname, port] ->
        %{locations: [{hostname, String.to_integer(port)}], host: server_name, identity: hostname}

      _ ->
        nil
    end
  end

  defp via_wellknown(server_name, cache) do
    # 3. If the hostname is not an IP literal, a regular HTTPS request is made
    # to https://<hostname>/.well-known/matrix/server, expecting the schema
    # defined later in this section. 30x redirects should be followed, however
    # redirection loops should be avoided. Responses (successful or otherwise)
    # to the /.well-known endpoint should be cached by the requesting
    # server. Servers should respect the cache control headers present on the
    # response, or use a sensible default when headers are not present. The
    # recommended sensible default is 24 hours. Servers should additionally
    # impose a maximum cache time for responses: 48 hours is
    # recommended. Errors are recommended to be cached for up to an hour, and
    # servers are encouraged to exponentially back off for repeated
    # failures. The schema of the /.well-known request is later in this
    # section. If the response is invalid (bad JSON, missing properties,
    # non-200 response, etc), skip to step 4. If the response is valid, the
    # m.server property is parsed as <delegated_hostname>[:<delegated_port>]
    # and processed as follows:
    # FIXME: sanitize server name
    well_known =
      case Polyjuice.Server.Federation.Discover.Cache.get(cache, server_name) do
        nil ->
          url =
            %URI{
              host: server_name,
              scheme: "https",
              port: 443,
              path: "/.well-known/matrix/server"
            }
            |> to_string()

          case :hackney.request(:get, url, [], "", follow_redirect: true) do
            {:ok, 200, _headers, client_ref} ->
              with {:ok, body} <- :hackney.body(client_ref),
                   {:ok, json} <- Jason.decode(body) do
                # FIXME: get the cache lifetime from header
                res = {:ok, json}
                Polyjuice.Server.Federation.Discover.Cache.put(cache, server_name, res, 86400)
                res
              else
                e ->
                  res = {:error, e}
                  Polyjuice.Server.Federation.Discover.Cache.put(cache, server_name, res, 86400)
                  res
              end

            {:ok, status, _, _} ->
              res = {:error, status}
              Polyjuice.Server.Federation.Discover.Cache.put(cache, server_name, res, 3600)
              res

            _ ->
              # FIXME: ???
              res = {:error, :unknown}
              Polyjuice.Server.Federation.Discover.Cache.put(cache, server_name, res, 3600)
              res
          end

        res ->
          res
      end

    # - If <delegated_hostname> is an IP literal, then that IP address should
    #   be used together with the <delegated_port> or 8448 if no port is
    #   provided. The target server must present a valid TLS certificate for
    #   the IP address. Requests must be made with a Host header containing the
    #   IP address, including the port if one was provided.

    # - If <delegated_hostname> is not an IP literal, and <delegated_port> is
    #   present, an IP address is discovered by looking up an AAAA or A record
    #   for <delegated_hostname>. The resulting IP address is used, alongside
    #   the <delegated_port>. Requests must be made with a Host header of
    #   <delegated_hostname>:<delegated_port>. The target server must present a
    #   valid certificate for <delegated_hostname>.

    # - If <delegated_hostname> is not an IP literal and no <delegated_port> is
    #   present, an SRV record is looked up for
    #   _matrix._tcp.<delegated_hostname>. This may result in another hostname
    #   (to be resolved using AAAA or A records) and port. Requests should be
    #   made to the resolved IP address and port with a Host header containing
    #   the <delegated_hostname>. The target server must present a valid
    #   certificate for <delegated_hostname>.

    # - If no SRV record is found, an IP address is resolved using AAAA or A
    #   records. Requests are then made to the resolve IP address and a port of
    #   8448, using a Host header of <delegated_hostname>. The target server
    #   must present a valid certificate for <delegated_hostname>.

    case well_known do
      {:ok, %{"m.server" => delegated_hostname}} ->
        as_ip_literal(delegated_hostname) || as_hostname_with_port(delegated_hostname) ||
          via_srv(delegated_hostname) ||
          %{
            locations: [{delegated_hostname, 8448}],
            host: delegated_hostname,
            identity: delegated_hostname
          }

      {:ok, %{}} ->
        # FIXME: ???
        nil

      {:error, _} ->
        nil
    end
  end

  defp via_srv(server_name) do
    # 4. If the /.well-known request resulted in an error response, a server is
    # found by resolving an SRV record for _matrix._tcp.<hostname>. This may
    # result in a hostname (to be resolved using AAAA or A records) and
    # port. Requests are made to the resolved IP address and port, using 8448
    # as a default port, with a Host header of <hostname>. The target server
    # must present a valid certificate for <hostname>.

    srv = :inet_res.lookup('_matrix._tcp.' ++ to_charlist(server_name), :in, :srv)

    case srv do
      [] ->
        nil

      _ ->
        locations =
          srv
          |> Enum.sort(fn {prio1, wt1, _, _}, {prio2, wt2, _, _} ->
            # FIXME: use weight correctly (e.g. within the same priority,
            # randomly order the results so that the results with higher weight
            # are more likely to appear earlier)
            prio1 < prio2 || (prio1 == prio2 && wt1 >= wt2)
          end)
          |> Enum.map(fn {_, _, port, target} -> {to_string(target), port} end)

        %{locations: locations, host: server_name, identity: server_name}
    end
  end

  # simple in-memory cache
  defmodule MemoryCache do
    @enforce_keys [:pid]
    defstruct [:pid]

    use Agent

    def start_link(opts \\ []) do
      Agent.start_link(fn -> %{} end, opts)
    end

    defimpl Polyjuice.Server.Federation.Discover.Cache do
      def get(%{pid: pid}, server_name) do
        Agent.get_and_update(pid, fn state ->
          case Map.get(state, server_name) do
            nil ->
              {nil, state}

            {val, time} ->
              if time > System.monotonic_time(:millisecond) do
                {nil, Map.delete(state, server_name)}
              else
                {val, state}
              end
          end
        end)
      end

      def put(%{pid: pid}, server_name, value, lifetime) do
        Agent.update(
          pid,
          &Map.put(
            &1,
            server_name,
            {value, System.monotonic_time(:millisecond) + lifetime * 1000}
          )
        )
      end
    end
  end
end
