# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.Server.SyncToken do
  @moduledoc """
  A sync token indicates the position the client is at in multiple streams.  It
  can be encoded as a string.

  """
  # sync tokens:
  # r: room events
  # d: to-device events
  # a: account data
  # e: ephemeral (presence/typing)
  # k: keys/device updates
  @opaque t() :: %__MODULE__{
            components: %{optional(String.t()) => non_neg_integer}
          }
  defstruct [
    :components
  ]

  @doc """
  Parse a sync token string.

  Example

      iex> Polyjuice.Server.SyncToken.parse("r123_d456_a789")
      {:ok, %Polyjuice.Server.SyncToken{
         components: %{
           a: 789,
           d: 456,
           r: 123
         }
       }}
  """
  @spec parse(token :: String.t()) :: {:ok, t()} | :error
  def parse("") do
    {:ok, %__MODULE__{components: %{}}}
  end

  def parse(token) when is_binary(token) do
    try do
      components =
        String.split(token, "_")
        |> Enum.map(fn <<sigil::binary-size(1), pos::binary>> ->
          {String.to_existing_atom(sigil), String.to_integer(pos)}
        end)
        |> Map.new()

      {:ok, %__MODULE__{components: components}}
    rescue
      err ->
        IO.puts(inspect(err))
        :error
    end
  end

  @doc """
  Stringify a sync token

  Example

      iex> Polyjuice.Server.SyncToken.to_string(%Polyjuice.Server.SyncToken{
      ...>   components: %{
      ...>     r: 123
      ...>   }
      ...> })
      "r123"

      iex> Polyjuice.Server.SyncToken.to_string(%Polyjuice.Server.SyncToken{
      ...>   components: %{
      ...>     a: 789,
      ...>     d: 456,
      ...>     r: 123
      ...>   }
      ...> })
      ...> |> Polyjuice.Server.SyncToken.parse()
      {:ok, %Polyjuice.Server.SyncToken{
         components: %{
           a: 789,
           d: 456,
           r: 123
         }
       }}
  """
  @spec to_string(token :: t() | nil) :: String.t()
  def to_string(%__MODULE__{} = token) do
    Enum.map(token.components, fn {sigil, pos} -> "#{sigil}#{pos}" end)
    |> Enum.join("_")
  end

  def to_string(nil) do
    "r0"
  end

  @spec is_newer(token :: t() | nil, component :: atom, position :: pos_integer) :: boolean
  def is_newer(%__MODULE__{} = token, component, position)
      when is_atom(component) and is_integer(position) and position >= 0 do
    Map.get(token.components, component, 0) < position
  end

  def is_newer(nil, component, position)
      when is_atom(component) and is_integer(position) and position >= 0 do
    true
  end

  @spec update(token :: t() | nil, component :: atom, position :: pos_integer) :: t()
  def update(%__MODULE__{} = token, component, position)
      when is_atom(component) and is_integer(position) and position >= 0 do
    %__MODULE__{
      components: Map.put(token.components, component, position)
    }
  end

  def update(nil, component, position)
      when is_atom(component) and is_integer(position) and position >= 0 do
    %__MODULE__{
      components: %{component => position}
    }
  end

  @spec get_component(token :: t() | nil, component :: atom) :: non_neg_integer
  def get_component(%__MODULE__{} = token, component) when is_atom(component) do
    Map.get(token.components, component, 0)
  end

  def get_component(nil, component) when is_atom(component) do
    0
  end

  defimpl String.Chars do
    def to_string(token), do: Polyjuice.Server.SyncToken.to_string(token)
  end
end
